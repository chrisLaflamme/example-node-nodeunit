var book = require('./lib/grades').book;
var express = require('express');
var app = express();

// '/' equals requst for root. i.e When localhost:3000/ send "Hello World!"
app.get('/', (req, res) => {
    // send response back
    res.send("Hello World!");
});

// i.e. When localhost:3000/grade send res.send args
app.get('/grade', (req, res) => {
    var grades = req.query.grades.split(',');

    for (var i = 0; i < grades.length; i = i + 1) {
        book.addGrade(parseInt(grades[i]));
    }
    var average = book.getAverage();
    var letter = book.getLetterGrade();

    //send a response through Express
    res.send("Your average grade is " + average + ". You earned a " + letter + ".");
});

app.listen(3000);

console.log("Server listening on port 3000...");