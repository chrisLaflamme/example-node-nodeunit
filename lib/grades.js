var gradeBook = {
    _grades: [],
    addGrade: function(newGrade) {
        this._grades.push(newGrade);
    },

    getCountOfGrades: function() {
        return this._grades.length;
    },

    getAverage: function() {
        var totalGrades = 0;
        for(var i =0; i < this._grades.length; i += 1) {
            totalGrades += this._grades[i];
        }

        return totalGrades / this._grades.length;
    },

    getLetterGrade: function() {
        var letterGrade = '';
        var numberGrade = this.getAverage();
        if (numberGrade >= 90) {
            letterGrade = 'A';
        } else if (numberGrade >= 80) {
            letterGrade = 'B'
        } else if (numberGrade >= 70) {
            letterGrade = 'C'
        } else if (numberGrade >= 60) {
            letterGrade = 'D';
        } else {
            letterGrade = 'F';
        }
        return letterGrade;
    },

    reset: function() {     // clear out vals in grades for test running
        this._grades = [];
    }
};

exports.book = gradeBook;
