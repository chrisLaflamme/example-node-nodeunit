var book = require('../lib/grades').book;

//"setUp" is run prior to every test and can / should be used to clean the env
exports["setUp"] = function(callback) {
    book.reset();
    callback();
};

// different tests are defined in exports[<test name>]
exports['Can add new grade'] = function(test) {
    book.addGrade(90);

    var count = book.getCountOfGrades();

    test.equal(count, 1);
    test.done();
};

exports['Can average grades'] = function(test) {
    book.addGrade(100);
    book.addGrade(50);

    var average = book.getAverage();

    test.equal(average, 75);
    test.done();
};

exports['Can computer letter grade A'] = function(test) {
    book.addGrade(100);
    book.addGrade(90);

    var result = book.getLetterGrade();
    
    test.equal(result, 'A');
    test.done();
};

exports['50 is a failing grade'] = function(test) {
    book.addGrade(50);

    var result = book.getLetterGrade();

    test.equal(result, 'F');
    test.done();
};